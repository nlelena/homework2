# Subiectul 1: HTML

## Sa se defineasca 3 fisiere cu extensia `html` cu urmatoarele denumiri `home.html`, `contact.html`, `about.html`.

## Fisierul `home.html` va avea urmatoarea structura:
- O imagine pe post de logo (se va folosi ca si sursa imaginea gasita in directorul `assets/imgs/logo.png`);
- Un element de tip `div` ce contine doua ancore ce vor redirectina utilizatorul catre alte doua pagini html: `contact.html`, `about.html`;

## Fisierul `contact.html` va avea urmatoarea structura:
- `div` cu id-ul `form-container` ce va ocupa 40% din latimea ecranului si va fi afisat in centrul acestuia;
- In interiorul `div-ului` se vor adauga 2 elemente de tip `input` cu placeholder-ul `Nume` si `Prenume`;
- In interiorul `div-ului` se va adauga un element de tip `button` cu urmatoarele proprietati de stilizare:
    + Dimensiunea fontului de 20px;
    + Border solid de culoare neagra;
    + Border radius de 15px;

## Fisierul `about.html` va avea urmatoarea structura: 
- Titlu principal cu continutul: `About US`;
- Element de tip lista neordonata ce contin 3 elemente: `Quality services`, `Great team`, `Hard workers`;

